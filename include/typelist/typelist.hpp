#ifndef TYPELIST_H_
#define TYPELIST_H_

#include <utility>

namespace typelist {

template <typename ... Types>
struct TypeList{};

// ===

template <typename List>
struct Size;

template <typename ... T>
struct Size<TypeList<T...>> {
    static constexpr std::size_t value = sizeof...(T);
};

template <typename List>
inline constexpr std::size_t SizeV = Size<List>::value;

// ===

template <typename List>
struct Head;

template <typename H, typename ... T>
struct Head<TypeList<H, T...>> {
    using type = H;
};

template <typename List>
using HeadT = typename Head<List>::type;

// ===

template <typename List>
struct Tail;

template <typename H, typename ... T>
struct Tail<TypeList<H, T...>> {
    using type = TypeList<T...>;
};

template <typename List>
using TailT = typename Tail<List>::type;

// ===

template <typename List>
struct Last;

template <typename T>
struct Last<TypeList<T>> {
    using type = T;
};

template <typename H, typename ... T>
struct Last<TypeList<H, T...>> {
    using type = typename Last<TailT<TypeList<H, T...>>>::type;
};

template <typename List>
using LastT = typename Last<List>::type;

// ===

template <typename List, std::size_t S>
struct Subscript;

template <typename T, typename ... Types>
struct Subscript<TypeList<T, Types...>, 0> {
	using type = T;
};

template <typename T, typename ... Types, std::size_t S>
struct Subscript<TypeList<T, Types...>, S> {
	using type = typename Subscript<TypeList<Types...>, S - 1>::type;
};

template <typename List, std::size_t S>
using SubscriptT = typename Subscript<List, S>::type;

// ===

template <typename List, typename T>
struct Index;

template <typename T, typename ... Types>
struct Index<TypeList<T, Types...>, T> {
    constexpr static std::size_t value = 0;
};

template <typename T1, typename T2, typename ... Types>
struct Index<TypeList<T2, Types...>, T1> {
    constexpr static std::size_t value = 1 + Index<TypeList<Types...>, T1>::value;
};

template <typename List, typename T>
inline constexpr std::size_t IndexV = Index<List, T>::value;

// ===

template <typename List, typename T>
struct Append;

template <typename T, typename ...Types>
struct Append<TypeList<Types...>, T> {
    using type = TypeList<Types..., T>;
};

template <typename List, typename T>
using AppendT = typename Append<List, T>::type;

template <typename List, typename T>
struct Prepend;

template <typename T, typename ...Types>
struct Prepend<TypeList<Types...>, T> {
    using type = TypeList<T, Types...>;
};

template <typename List, typename T>
using PrependT = typename Prepend<List, T>::type;

} // typelist


#endif /* TYPELIST_H_ */
