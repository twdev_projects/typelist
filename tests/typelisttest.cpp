#include <typelist/typelist.hpp>

#include <type_traits>

using namespace typelist;

using List1 = TypeList<int>;
using List2 = TypeList<float, short>;
using List3 = TypeList<int, float, std::string>;
using FourInts = TypeList<int, int, int, int>;

static_assert(std::is_same_v<Head<List1>::type, int>, "");
static_assert(std::is_same_v<Head<List2>::type, float>, "");
static_assert(std::is_same_v<Head<List3>::type, int>, "");
static_assert(std::is_same_v<HeadT<List1>, int>, "");
static_assert(std::is_same_v<HeadT<List2>, float>, "");
static_assert(std::is_same_v<HeadT<List3>, int>, "");

static_assert(Size<List1>::value == 1, "");
static_assert(Size<List2>::value == 2, "");
static_assert(Size<List3>::value == 3, "");
static_assert(SizeV<List1> == 1, "");
static_assert(SizeV<List2> == 2, "");
static_assert(SizeV<List3> == 3, "");

static_assert(std::is_same_v<Last<List1>::type, int>, "");
static_assert(std::is_same_v<Last<List2>::type, short>, "");
static_assert(std::is_same_v<Last<List3>::type, std::string>, "");
static_assert(std::is_same_v<LastT<List1>, int>, "");
static_assert(std::is_same_v<LastT<List2>, short>, "");
static_assert(std::is_same_v<LastT<List3>, std::string>, "");

static_assert(std::is_same_v<Subscript<List3, 0>::type, int>, "");
static_assert(std::is_same_v<Subscript<List3, 1>::type, float>, "");
static_assert(std::is_same_v<Subscript<List3, 2>::type, std::string>, "");
static_assert(std::is_same_v<SubscriptT<List3, 0>, int>, "");
static_assert(std::is_same_v<SubscriptT<List3, 1>, float>, "");
static_assert(std::is_same_v<SubscriptT<List3, 2>, std::string>, "");

static_assert(Index<List1, int>::value == 0, "");
static_assert(Index<List3, float>::value == 1, "");
static_assert(Index<List3, std::string>::value == 2, "");
static_assert(Index<FourInts, int>::value == 0, "");
static_assert(IndexV<List1, int> == 0, "");
static_assert(IndexV<List3, float> == 1, "");
static_assert(IndexV<List3, std::string> == 2, "");
static_assert(IndexV<FourInts, int> == 0, "");

using FiveInts = Append<FourInts, int>::type;

using SixInts = Prepend<FiveInts, int>::type;

using ShortLast = Append<List3, short>::type;

using ShortFirst = Prepend<List3, short>::type;

static_assert(Size<FiveInts>::value == 5, "");
static_assert(Size<SixInts>::value == 6, "");

static_assert(Size<ShortFirst>::value == 4, "");
static_assert(Size<ShortLast>::value == 4, "");
static_assert(std::is_same_v<Head<ShortFirst>::type, short>, "");
static_assert(std::is_same_v<Last<ShortLast>::type, short>, "");


int main(int, const char *[])
{
    // This is a compile time test
    return 0;
}
